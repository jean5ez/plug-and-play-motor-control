# Plug and play motor control

Predictive control of motors structured around characteristic model acquired from first-run step response using least square fit which is so lightweight it can run of most microcontrollers.  No pre-characterizing or parameter fine-tuning required.  
